import React from "react"
import {render} from "react-dom"
import {Router, Route, IndexRoute, browserHistory} from "react-router"
import injectTapEventPlugin from "react-tap-event-plugin"

import {App, Index, TestPage} from "./index"

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

render((
	<Router history={browserHistory}>
		<Route path="/" component={App}>
			<IndexRoute component={Index}/>
			<Route path="test" component={TestPage}/>
		</Route>
	</Router>
), document.getElementById("app"));
