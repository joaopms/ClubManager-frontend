import React, {Component} from "react"
import {Router, Link} from "react-router"

import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"
import AppBar from "material-ui/AppBar"

export class App extends Component {
	render() {
		return (
			<MuiThemeProvider>
				<div>
					<AppBar
					/>
					<Link to={"/test"}>Test1</Link>
					{this.props.children}
				</div>
			</MuiThemeProvider>
		)
	}
}

export class Index extends Component {
	render() {
		return (
			<h1>Index</h1>
		)
	}
}

export class TestPage extends Component {
	render() {
		return(
			<p>test :3</p>		
		)
	}
}
