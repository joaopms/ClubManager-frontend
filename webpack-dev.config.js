var webpack = require("webpack")

module.exports = {
	entry: {
		app: [
			"webpack/hot/dev-server",
			"webpack/hot/only-dev-server",
			"./src/js/app.js"
		],
		vendor: [
			"react",
			"react-dom",
			"react-router",
			"react-tap-event-plugin",
			"material-ui"
		]
	},
	output: {
		filename: "js/app.js"
	},
	devServer: {
		contentBase: "src",
		devtool: "eval",
		hot: true,
		inline: true,
		port: 3000,
		host: "0.0.0.0",
		historyApiFallback: true
	},
	plugins: [
		new webpack.optimize.CommonsChunkPlugin("vendor", "js/vendor.js"),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin()
	],
	module: {
		loaders: [
			{
				test: /\.js$/,
				loaders: ["react-hot", "babel-loader"],
				include: /src/
			}
		]
	}
}
